const { KohanaJS } = require('kohanajs');

KohanaJS.initConfig(new Map([
  ['helmet', require('./config/helmet')],
]));
